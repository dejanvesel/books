import { Component, OnDestroy, OnInit } from '@angular/core';
import { NguCarouselConfig } from '@ngu/carousel';

import { AuthService } from '../auth/auth.service';

import { LandingService } from './landing.service';
import { Subject, takeUntil } from 'rxjs';
import { BooksModel } from './books-model';
import { SharedService } from '../shared.service';
import { Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss'],
})

export class LandingPageComponent implements OnInit, OnDestroy {
  images = [
  ];


  public carouselTileItems: Array<any> = [0];
  public carouselTiles: any  = {
    0: [],
  };
  public carouselTile: NguCarouselConfig = {
    grid: { xs: 2, sm: 2, md: 3, lg: 3, xl: 3, all: 0 },
    slide: 1,
    speed: 550,
    point: {
      visible: true,
    },
    load: 3,
    touch: true,
    easing: 'cubic-bezier(0, 0, 0.2, 1)',
  };
  logedIn: any;


  constructor(private _authService: AuthService,
    private LandingService: LandingService,
    private SharedService: SharedService) { }

  ngOnInit(): void {
    this._authService.checkLogedInStatus();
    this._authService.logedIn.subscribe(
      response => {
        this.logedIn = response;
        return true;
      }
    )

    this.carouselTileItems.forEach(el => {
      this.carouselTileLoad(el);
    });

    this.LandingService.getData()
    .pipe(takeUntil(this.ngUnsubscribe))
    .subscribe((result: BooksModel[]) => {
      console.log('The Data: ', result);
      this.theData = result;
    });
}

public carouselTileLoad(j: any) {
  const len = this.carouselTiles[j].length;
  if (len <= 1) {
    for (let i = len; i < len + 6; i++) {
      this.carouselTiles[j].push(this.images[i]);
    }}}
  theData: BooksModel[] = [];

  addedToCart = [];

  private ngUnsubscribe: Subject<void> = new Subject<void>();

  clickMe() {
    this.SharedService.sendClickEvent();
  }

  calcRaitingStars(raiting: number): number[] {
    const totalStars: number = Math.floor(raiting);

    const decimalNumber: number = raiting % 1;

    let newArray: number[] = Array(totalStars).fill(1);
    if (decimalNumber > 0) {
      newArray.push(0.5);
    }

    return newArray;
  }

  addToCart(book: BooksModel): void {
    book.quantity = 1;
    this.SharedService.addBookToCart(book);
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.unsubscribe();
  }
}