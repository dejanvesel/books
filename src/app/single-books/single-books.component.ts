import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'single-books',
  templateUrl: './single-books.component.html',
  styleUrls: ['./single-books.component.scss']
})
export class SingleBooksComponent implements OnInit {

  logedIn: boolean;
  
  constructor(private _authService: AuthService) { }

  ngOnInit(): void {this._authService.checkLogedInStatus();
    
    if(this._authService.logedIn && this._authService.logedIn != null ) {
      this.logedIn = true;
    }  
  }

}
