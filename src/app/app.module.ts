import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { SingleBooksComponent } from './single-books/single-books.component';
import { FooterComponent } from './components/footer/footer.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { AboutComponent } from './about/about.component';
import { ErrorComponent } from './error/error.component';
import { MostPopularComponent } from './most-popular/most-popular.component';
import { HeaderComponent } from './components/header/header.component';
import { AuthComponent } from './auth/auth.component';
import { SharedModule } from './shared/shared.module';
import { CommonModule } from '@angular/common';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';

@NgModule({
  declarations: [
    AppComponent,
    SingleBooksComponent,
    FooterComponent,
    LandingPageComponent,
    ErrorComponent,
    AboutComponent,
    MostPopularComponent,
    HeaderComponent,
    AuthComponent,
    ShoppingCartComponent,
  ],
  imports: [
    CommonModule,
    SharedModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
