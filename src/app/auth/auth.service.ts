import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { logedInCheck, Login, User } from '../app.model';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  usersSubject: BehaviorSubject<User> = new BehaviorSubject(new User);
  usersList: User[] = [];
  logedIn: BehaviorSubject<boolean> = new BehaviorSubject(null);

  constructor( 
    private _router : Router,
    private _httpClient : HttpClient ) { }

  public createAccount(filter: User) : Observable<User> {
    return this._httpClient.post<User>('https://api.bookexchange.techup.me/account', filter);
  }

  public verifyAcc(verify_key: any) {
    return this._httpClient.put(`https://api.bookexchange.techup.me/account/verify?accountKey=${verify_key}`, verify_key);
  }

  public login(filter: Login) : Observable<Login> {
    return this._httpClient.post<Login>('https://api.bookexchange.techup.me/auth/login', filter);
  }

  public checkEmail(email: any) {
    return this._httpClient.get(`https://api.bookexchange.techup.me/auth/password/forgot?email=${email}`);
  }

  public resetPassword(data: any) {
    return this._httpClient.put('https://api.bookexchange.techup.me/auth/pasword/reset', data);
  }

  public getRefreshToken(key: any){
    let header = new HttpHeaders({
      'Authorization': `${key}`
    });
    return this._httpClient.get('https://api.bookexchange.techup.me/auth/authorize', {headers: header});
  }

  checkLogedInStatus() {
    let storage: any;
    if(localStorage.getItem('loged-in-token') && localStorage.getItem('loged-in-token') != null){
      storage = localStorage.getItem('loged-in-token');
      this.logedIn.next(true);
    } else {
      this.logedIn.next(false);
    }
  }

  registerUser(newUser: User) {
    let users = localStorage.getItem('usersList');
    if (users) {
      this.usersList = JSON.parse(users);
    }

    this.usersList.push(newUser);

    localStorage.setItem('usersList', JSON.stringify(this.usersList));

    alert("Registration is successfull");

    setTimeout(() => {
      this._router.navigate(['auth/login']);
    }, 500)
  };

  loginUser(userData: User) {
    let users = [];
    let lc = localStorage.getItem('usersList');
    if (lc) {
      users = JSON.parse(lc);

      let userMatch = users.find((user => user.email == userData.email) && (user => user.password == userData.password));
      if (userMatch) {
        this.usersSubject.next(userMatch);

        localStorage.setItem('activeUser', JSON.stringify(userMatch));

        // this._router.navigate(['../home']);
      } else {
        alert('Wrong Credentials!');
      }
    }
  };

  checkPreviouslyLoggedUser() {
    let activeUser = localStorage.getItem('activeUser');
    if (activeUser) {
      this.usersSubject.next(JSON.parse(activeUser));
      return true;
    } else {
      return false;
    }
  };

  logOut() {
    localStorage.removeItem('activeUser');
    localStorage.removeItem('loged-in-token'); 
    this.usersSubject.next(new User);
    this.checkLogedInStatus();
    this._router.navigate(['../home']);
  }
}
