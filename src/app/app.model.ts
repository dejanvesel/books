export class User {
    firstName ?: string;
    lastName ?: string;
    email ?: string;
    password ?: string;
    phone ?: number;
}

export class Login {
    email ?: string;
    password ?: string;
}

export class changePass {
    name ?: string;
    phone ?: number;
    email ?: string;
    dob ?: string;
    password ?: string;
}

export class resetPassword {
    token ?: string;
    password ?: string;
}

export class logedInCheck {
    checked ?: any;
}