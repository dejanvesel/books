import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/app.model';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm: FormGroup;
  newUser = new User();
  registerLogedIn: boolean;

  constructor(
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _authService: AuthService,
  ) { 
    this.registerForm = this._formBuilder.group({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('', Validators.required),
      email: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      phone: new FormControl(''),
    });
  }

  ngOnInit(): void {
  }

  saveForm(): void {
    this.newUser.firstName = this.registerForm.get('firstName').value;
    this.newUser.lastName = this.registerForm.get('lastName').value;
    this.newUser.email = this.registerForm.get('email').value;
    this.newUser.password = this.registerForm.get('password').value;
    this.newUser.phone = this.registerForm.get('phone').value;

    this._authService.registerUser(this.newUser);
    
    this._authService.createAccount(this.newUser).subscribe(
      data => {
        this._router.navigate(['../verify']);
      }
    )
  }
}
