import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BooksModel } from './books-model';
import { Observable, Subject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class LandingService {

  constructor(private httpClient: HttpClient) { }

   getData() : Observable<BooksModel[]>{
  return this.httpClient.get<BooksModel[]>('https://api.npoint.io/b420c982f95f560653b7');
 }


 }



 

