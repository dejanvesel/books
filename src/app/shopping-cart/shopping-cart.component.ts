import { Component, OnInit } from '@angular/core';
import { reduce, takeUntil } from 'rxjs/operators';
import { BooksModel } from '../landing-page/books-model';
import { SharedService } from '../shared.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-shopping-cart',
  templateUrl: './shopping-cart.component.html',
  styleUrls: ['./shopping-cart.component.scss'],
})
export class ShoppingCartComponent implements OnInit {
  addedToCart$: Observable<BooksModel[]>;

  total:number = 0;
  ngUnsubscribe: any;

  constructor(private SharedService: SharedService) {}

  ngOnInit(): void {
    this.addedToCart$ = this.SharedService.addedToCart;

    this.addedToCart$
        .pipe(takeUntil(this.ngUnsubscribe))
        .subscribe((addedBooks: BooksModel[]) => {
          console.log(addedBooks);
            this.total = addedBooks.reduce(
                (total: number, currentBook: BooksModel) =>
                    currentBook.onSale
                        ? total +
                          currentBook.discountPrice * currentBook.quantity
                        : total +
                          currentBook["originalPrice:"] *
                              currentBook.quantity, 0
            );
        });
}

  onBookRemove(book: BooksModel): void {
    this.SharedService.removeBookFromCart(book);
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe.next();
    this.ngUnsubscribe.unsubscribe();
  }
}
