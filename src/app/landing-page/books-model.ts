export class BooksModel {
    
    id?: string;
    image: string;
    title: string;
    author: string;
    onSale: boolean;
    rating: number;
    inStock: number;
    description: string;
    discountPrice: number;
    originalPrice: number;
    quantity?: number;

}
