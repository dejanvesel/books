import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { Login } from 'src/app/app.model';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  loginObj = new Login;
  responseData: any;
  accessToken: any;
  loginLogedIn: boolean;

  constructor(
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _authService: AuthService,
    private _appComponent: AppComponent
  ) {
    this.loginForm = this._formBuilder.group({
      email: ['', [Validators.required]],
      password: ['', [Validators.required]],
    });
  }

  ngOnInit(): void {
    this._authService.checkLogedInStatus();
    this._authService.logedIn.subscribe(
      response => {
        this.loginLogedIn = response;
        return true;
      }
    )
  }

  saveForm() {
    this.loginObj.email = this.loginForm.get('email').value;
    this.loginObj.password = this.loginForm.get('password').value;

    this._authService.loginUser(this.loginObj);
    this._authService.login(this.loginObj).subscribe(
      data => {
        if(data && data != null){
          this.responseData = data;
          console.log(this.responseData)
          this.accessToken = this.responseData.response.accessToken;
          
          this._authService.getRefreshToken(this.accessToken).subscribe(
            response => {
              localStorage.setItem('loged-in-token', JSON.stringify(this.accessToken));
              this._router.navigate(['home']);
            }
          );
        }
      }, error => {
        alert("Please verify your email. Check your email");
        this._router.navigate(['home']);
      }
    );
  }
}
