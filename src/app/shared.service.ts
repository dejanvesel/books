import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable, Subject } from 'rxjs';
import { BooksModel } from './landing-page/books-model';
@Injectable({
  providedIn: 'root',
})
export class SharedService {
  private _addedToCart = new BehaviorSubject<BooksModel[]>([]);
  addedToCart = this._addedToCart.asObservable();

  private subject = new Subject<any>();

  addBookToCart(book: BooksModel): void {
    const currentValue: BooksModel[] = this._addedToCart.value;
    const newCartArray: BooksModel[] = [...currentValue, book];
    this._addedToCart.next(newCartArray);
  }

  removeBookFromCart(book: BooksModel): void {
    const currentValue: BooksModel[] = this._addedToCart.value;
    const newCartArray: BooksModel[] = currentValue.filter(
      (currentBook: BooksModel) => currentBook.id !== book.id
    );

    this._addedToCart.next(newCartArray);
  }

  sendClickEvent() {
    this.subject.next(null);
  }
  getClickEvent(): Observable<any> {
    return this.subject.asObservable();
  }
}
