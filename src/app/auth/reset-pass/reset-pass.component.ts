import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { changePass, resetPassword } from 'src/app/app.model';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-reset-pass',
  templateUrl: './reset-pass.component.html',
  styleUrls: ['./reset-pass.component.scss']
})
export class ResetPassComponent implements OnInit {

  resetClick: boolean = false;
  email: any;
  newPass: any;
  resetPass: FormGroup;
  newPassObj = new changePass;
  mailCheck: FormGroup;
  resetPassObj = new resetPassword;
  resetLogedIn: boolean;

  passwordsMatching: boolean = false;
  isConfirmPasswordDirty: boolean = false;
  confirmPasswordClass: string = 'form-control';

  resetPWToken: string;

  constructor(
    private _formBuilder: FormBuilder,
    private _router: Router,
    private _authService: AuthService,
    private _route: ActivatedRoute,
  ) {

    this.resetPass = this._formBuilder.group({
      oldPass: ['', [Validators.required]],
      newPass: ['', [Validators.required, Validators.pattern('((?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,})')]],
      confirmPass: ['', [Validators.required]],
    })

    this.mailCheck = this._formBuilder.group({
      email: ['', [Validators.required]],
    })
  }

  ngOnInit(): void {
    this._route.queryParams.subscribe(
      params => {
        if (params['token']) {
          this.resetPWToken = params['token'];
          this.resetPassObj.token = this.resetPWToken;
          this.resetClick = true;
          // api reqest za reset passwrod

          // navigiras na login ( prosledi go tokenot povtorno vo query params )
        }
      }
    );
  
  }

  checkPasswords(pw: string, cpw: string) {
    this.isConfirmPasswordDirty = true;
    if (pw === cpw) {
      this.passwordsMatching = true;
      this.confirmPasswordClass = 'form-control is-valid';
      this.resetPass.valid
    } else {
      this.passwordsMatching = false;
      this.confirmPasswordClass = 'form-control is-invalid'
    }
  }

  checkOldPass(op: any) {
    if (op) {
      let old1 = localStorage.getItem('usersList');
      let old = JSON.parse(old1);
      if(op === old[0].password) {
        this.resetPass.enable;
      } else {
        this.resetPass.disable;
      }
    }
  }

  checkEmail(email: any) {
    this.email = email;
  }

  sendEmail() {
    this._authService.checkEmail(this.email).subscribe(
      (result: any) => {
        if(result) {
          this.resetClick = true;
        }
      }
    );
  }

  saveNewPass() {
    this.newPassObj = JSON.parse(localStorage.getItem('usersList'));
    this.newPassObj[0].password = this.resetPass.get('newPass').value;
        
    localStorage.setItem('usersList', JSON.stringify(this.newPassObj));
    
    if(this.resetPWToken) {
      this.resetPassObj.password = this.resetPass.get('newPass').value;
      console.log(this.resetPassObj);

      this._authService.resetPassword(this.resetPassObj).subscribe (
        (result: any) => {

          this._router.navigate(
            ['auth/login'],
            { queryParams: { token: this.resetPWToken } }
          );
        }
      )
    }
    // this._router.navigate(['../login']);
  }
}
