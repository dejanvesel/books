import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { AfterViewInit, Component, HostBinding, OnInit } from '@angular/core';

import { AuthService } from 'src/app/auth/auth.service';
import { SharedService } from 'src/app/shared.service';
import { distinctUntilChanged, filter, fromEvent, map, Observable, pairwise, share, Subscription, throttleTime } from 'rxjs';
import { BooksModel } from 'src/app/landing-page/books-model';
import { Router } from '@angular/router';

enum VisibilityState {
  Visible = 'visible',
  Hidden = 'hidden',
}

enum Direction {
  Up = 'Up',
  Down = 'Down',
}

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  animations: [
    trigger('toggle', [
      state(
        VisibilityState.Hidden,
        // style({ opacity: 0, transform: 'translateY(-100%)' })
        style({ height: '60px' })
      ),
      state(
        VisibilityState.Visible,
        style({ opacity: 1, transform: 'translateY(0)' })
      ),
      transition('* => *', animate('200ms ease-in')),
    ]),
  ],
})
export class HeaderComponent implements OnInit {
  activeUser: any;

  logedIn: boolean = false;

  clickEventsubscription: Subscription;

  addedToCart$: Observable<BooksModel[]>;

  constructor(
    private _authService: AuthService,
    private sharedService: SharedService
  ) {
    this.clickEventsubscription = this.sharedService
      .getClickEvent()
      .subscribe(() => {
        this.incrementCount();
      });
  }

  count: number = 0;

  incrementCount() {
    this.count++;
  }

  ngOnInit(): void {
    this.addedToCart$ = this.sharedService.addedToCart;
    this._authService.checkPreviouslyLoggedUser();

    // Слушај кога ќе се испратат податоци на овој BehaviorSubject
    this._authService.usersSubject.subscribe((data) => {
      this.activeUser = data;
    });
  }

  logOut() {
    this._authService.logOut();
  }

  private isVisible = true;

  @HostBinding('@toggle')
  get toggle(): VisibilityState {
    return this.isVisible ? VisibilityState.Visible : VisibilityState.Hidden;
  }

  ngAfterViewInit() {
    const scroll$ = fromEvent(window, 'scroll').pipe(
      throttleTime(10),
      map(() => window.pageYOffset),
      pairwise(),
      map(([y1, y2]): Direction => (y2 < y1 ? Direction.Up : Direction.Down)),
      distinctUntilChanged(),
      share()
    );

    const goingUp$ = scroll$.pipe(
      filter((direction) => direction === Direction.Up)
    );

    const goingDown$ = scroll$.pipe(
      filter((direction) => direction === Direction.Down)
    );

    goingUp$.subscribe(() => (this.isVisible = true));
    goingDown$.subscribe(() => (this.isVisible = false));
  }

    notify = false;
 

  // AddToCart(){
  //   this.count++;
  //   this.notify = true;
  //   setTimeout(() => {
  //     this.notify = false;
  //   }, 300);
  // }
}
