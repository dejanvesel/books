import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-verify',
  templateUrl: './verify.component.html',
  styleUrls: ['./verify.component.scss']
})
export class VerifyComponent implements OnInit {
  
  constructor( private _authService: AuthService, private _route: ActivatedRoute, private _router: Router, private _appComponent: AppComponent) { }

  token: any;
  ngOnInit(): void {
    // alert("Please verify your email address. You should recieve an email!");
    this.getToken();
  }

  getToken(){
    this._route.queryParams.subscribe(
      params => {
        if(params['key'] && params['key'] != null){
          this.token = params['key']
          this._authService.verifyAcc(this.token).subscribe(
            data => {
            this._router.navigate(['auth/login']); 
            }, error => {
              this._router.navigate(['404']); 
            }
          );
        } else {
        }
      }
    )
  }
}
