import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth/auth.service';

@Component({
  selector: 'app-most-popular',
  templateUrl: './most-popular.component.html',
  styleUrls: ['./most-popular.component.scss']
})
export class MostPopularComponent implements OnInit {

  logedIn: boolean;
  
  constructor(private _authService: AuthService) { }

  ngOnInit(): void {this._authService.checkLogedInStatus();
    
    if(this._authService.logedIn && this._authService.logedIn != null ) {
      this.logedIn = true;
    }   
  }
}
